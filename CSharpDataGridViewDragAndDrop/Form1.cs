﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// 参考

// 2つのdatagridview間のドラッグ&ドロップによるデータの移動するには
// https://social.msdn.microsoft.com/Forums/ja-JP/9fa9aaf1-1380-4a98-83c9-88b1fcd4c723/21238812398datagridview382911239812489125211248312464amp1248912525?forum=csharpgeneralja

// DataGridViewのドラッグ&ドロップでデータ（行）を移動させたい
// https://social.msdn.microsoft.com/Forums/ja-JP/e59a4043-6298-4653-809f-5c8fcf04f2e6/datagridview1239812489125211248312464amp12489125251248312503123911?forum=csharpgeneralja

namespace CSharpDataGridViewDragAndDrop
{
    public partial class Form1 : Form
    {
        /**
         * @brief コンストラクタ
         */
        public Form1()
        {
            InitializeComponent();
        }

        /**
         * @brief フォームがロードされた時に呼び出されます。
         * 
         * @param [in] sender フォーム
         * @param [in] e イベント
         */
        private void Form1_Load(object sender, EventArgs e)
        {
            // サンプルデータを設定します。
            // ドロップ元のDataGridView
            dataGridView1.ColumnCount = 3;
            dataGridView1.Columns[0].HeaderText = "値A";
            dataGridView1.Columns[1].HeaderText = "値B";
            dataGridView1.Columns[2].HeaderText = "値C";
            dataGridView1.Rows.Add("A-1", "B-1", "C-1");
            dataGridView1.Rows.Add("A-2", "B-2", "C-2");
            dataGridView1.Rows.Add("A-3", "B-3", "C-3");

            // 選択モードを行全体に設定します。
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            // ドロップ先のDataGridView
            dataGridView2.ColumnCount = 3;
            dataGridView2.Columns[0].HeaderText = "値X";
            dataGridView2.Columns[1].HeaderText = "値Y";
            dataGridView2.Columns[2].HeaderText = "値Z";
            dataGridView2.Rows.Add("X-1", "Y-1", "Z-1");
            dataGridView2.Rows.Add("X-2", "Y-2", "Z-2");
            dataGridView2.Rows.Add("X-3", "Y-3", "Z-3");

            // ドロップを許可します。
            dataGridView2.AllowDrop = true;
        }

        /**
         * @brief dataGridView1でマウスが移動した時に呼び出されます。
         *
         * @param [in] sender マウス
         * @param [in] e イベント
         */
        private void dataGridView1_MouseMove(object sender, MouseEventArgs e)
        {
            // マウス座標のDataGridViewの位置情報を取得します。
            var hitTest = dataGridView1.HitTest(e.X, e.Y);

            // 念のため、有効なセル上でのみ許可する
            int newRowIndex = dataGridView1.NewRowIndex;
            int rowIndex = hitTest.RowIndex;
            if ((hitTest.Type == DataGridViewHitTestType.Cell)
                && ((newRowIndex == -1) || (newRowIndex != rowIndex)))
            {
                // ドラッグアンドドロップ動作を開始します。
                var row = dataGridView1.Rows[rowIndex];
                dataGridView1.DoDragDrop(row, DragDropEffects.Copy);
            }
        }

        /**
         * @brief ドラッグされた場合呼び出されます。
         * 
         * @param [in] sender 対象のコントロール
         * @parma [in] e イベント
         */
        private void dataGridView2_DragEnter(object sender, DragEventArgs e)
        {
            // ドラッグアンドドロップのドロップ効果をコピーに設定します。
            e.Effect = DragDropEffects.Copy;
        }

        /**
         * @brief ドラッグアンドドロップでドロップされた時に呼び出されます。
         * 
         * @param [in] sender 対象のコントロール
         * @parma [in] e イベント
         */
        private void dataGridView2_DragDrop(object sender, DragEventArgs e)
        {
            // ドロップ元(dataGridView1)のデータを取得します。
            var row = (DataGridViewRow)e.Data.GetData(typeof(DataGridViewRow));
            var cellDataNum = row.Cells.Count;
            var cellData = new object[cellDataNum];
            for (int column = 0; column < cellDataNum; ++column)
            {
                cellData[column] = row.Cells[column].Value;
            }

            // ドロップ先(dataGridView2)のクライアント位置からDataGridViewの位置情報を取得します。
            var point = dataGridView2.PointToClient(new Point(e.X, e.Y));
            var hitTest = dataGridView2.HitTest(point.X, point.Y);

            // ドロップ先(dataGridView2)の行位置を取得します。
            int rowIndex = hitTest.RowIndex;

            // ドロップ先(dataGridView2)の行位置がヘッダー行では無い場合
            if (rowIndex != -1)
            {
                // 該当行に挿入します。
                dataGridView2.Rows.Insert(rowIndex, cellData);
            }

            // ドロップ先(dataGridView2)の行位置がヘッダー行の場合
            else
            {
                // 末尾に行を追加します。
                dataGridView2.Rows.Add(cellData);
            }

            // 移動する場合、元のdataGridView1から該当行を削除します。
            row.DataGridView.Rows.Remove(row);
        }
    }
}
